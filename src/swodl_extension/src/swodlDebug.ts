/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/

import {
	Logger, logger,
	DebugSession,
	InitializedEvent, TerminatedEvent, StoppedEvent, BreakpointEvent, OutputEvent,
	Thread, StackFrame, Scope, Source, Handles, Breakpoint
} from 'vscode-debugadapter';
import { DebugProtocol } from 'vscode-debugprotocol';
import { basename } from 'path';
import { SwodlRuntime, MockBreakpoint } from './swodlRuntime';
const { Subject } = require('await-notify');

function timeout(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * This interface describes the mock-debug specific launch attributes
 * (which are not part of the Debug Adapter Protocol).
 * The schema for these attributes lives in the package.json of the mock-debug extension.
 * The interface should always match this schema.
 */
interface LaunchRequestArguments extends DebugProtocol.LaunchRequestArguments {
	/** An absolute path to the "program" to debug. */
	program: string;
	/** Automatically stop target after launch. If not specified, target does not stop. */
	stopOnEntry?: boolean;
	/** enable logging the Debug Adapter Protocol */
	trace?: boolean;
}

export class SwodlDebugSession extends DebugSession {

	// we don't support multiple threads, so we can use a hardcoded ID for the default thread
	private static THREAD_ID = 1;

	// a Mock runtime (or debugger)
	public _runtime: SwodlRuntime;

	private _variableHandles = new Handles<string>();

	private _configurationDone = new Subject();

	private _cancelationTokens = new Map<number, boolean>();
	private _isLongrunning = new Map<number, boolean>();

	/**
	 * Creates a new debug adapter that is used for one debug session.
	 * We configure the default implementation of a debug adapter here.
	 */
	public constructor(config) {
		super();

		// this debugger uses zero-based lines and columns
		this.setDebuggerLinesStartAt1(false);
		this.setDebuggerColumnsStartAt1(false);

		this._runtime = new SwodlRuntime(config);

		// setup event handlers
		this._runtime.on('stopOnEntry', () => {
			this.sendEvent(new StoppedEvent('entry', SwodlDebugSession.THREAD_ID));
		});
		this._runtime.on('stopOnStep', () => {
			this.sendEvent(new StoppedEvent('step', SwodlDebugSession.THREAD_ID));
		});
		this._runtime.on('stopOnBreakpoint', () => {
			this.sendEvent(new StoppedEvent('breakpoint', SwodlDebugSession.THREAD_ID));
		});
		this._runtime.on('stopOnDataBreakpoint', () => {
			this.sendEvent(new StoppedEvent('data breakpoint', SwodlDebugSession.THREAD_ID));
		});
		this._runtime.on('pause', () => {
			this.sendEvent(new StoppedEvent('pause', SwodlDebugSession.THREAD_ID));
		});
		this._runtime.on('stopOnException', () => {
			// this.sendEvent(new BreakpointEvent('changed', <DebugProtocol.Breakpoint>{
			// 	verified: bp.verified,
			// 	line: bp.line,
			// 	message: "ERROROROROROORROOROR!!!"
			// }));
			//this.setExceptionBreakPointsRequest();
			// log("ERRRE ______");
			//return;
			// var response = <DebugProtocol.ErrorResponse>{
			// 	success: false,
			// 	message: "TEts",
			// 	body: { }
			// };
			// response.body.error = <DebugProtocol.Message>{ format:"ERRRE asfasf"};

			// //this.sendErrorResponse(response, 2018, 'debug adapter only supports native paths', null, 1);
			// // this.sendResponse(response);
			// this.sendErrorResponse(response, 2);
			// log("ERRRE asfasf");
			const e: DebugProtocol.StoppedEvent = <DebugProtocol.StoppedEvent>{
				body: {
					reason: "exception",
					text: "test",
					description: "Paused on exception",
					allThreadsStopped: true,
					threadId: SwodlDebugSession.THREAD_ID
				}
			};//('exception', MockDebugSession.THREAD_ID);
			// e.body.text = "ERRRRROROROOR";
			// e.body.description = "Paused on exception";
			//e.body.allThreadsStopped = true;
			this.sendEvent(e);
		});
		this._runtime.on('breakpointValidated', (bp: MockBreakpoint) => {
			this.sendEvent(new BreakpointEvent('changed', <DebugProtocol.Breakpoint>{ verified: bp.verified, id: bp.id }));
		});
		this._runtime.on('output', (text, filePath, line, column, category="console") => {
			const e: DebugProtocol.OutputEvent = new OutputEvent(`${text}\n`);
			e.body.source = this.createSource(filePath);
			e.body.line = this.convertDebuggerLineToClient(line);
			e.body.column = this.convertDebuggerColumnToClient(column);
			e.body.category = category;
			this.sendEvent(e);
		});
		this._runtime.on('end', () => {
			this.sendEvent(new TerminatedEvent());
		});
	}

	/**
	 * The 'initialize' request is the first request called by the frontend
	 * to interrogate the features the debug adapter provides.
	 */
	protected initializeRequest(response: DebugProtocol.InitializeResponse, args: DebugProtocol.InitializeRequestArguments): void {

		// build and return the capabilities of this debug adapter:
		response.body = response.body || {};

		// the adapter implements the configurationDoneRequest.
		response.body.supportsConfigurationDoneRequest = true;

		// make VS Code to use 'evaluate' when hovering over source
		response.body.supportsEvaluateForHovers = true;

		// make VS Code to show a 'step back' button
		response.body.supportsStepBack = false;//true

		// make VS Code to support data breakpoints
		response.body.supportsDataBreakpoints = true;//true

		// make VS Code to support completion in REPL
		response.body.supportsCompletionsRequest = true;
		response.body.completionTriggerCharacters = [ ".", "[" ];

		// make VS Code to send cancelRequests
		response.body.supportsCancelRequest = true;

		// make VS Code send the breakpointLocations request
		response.body.supportsBreakpointLocationsRequest = true;

		response.body.supportsEvaluateForHovers = true;
		response.body.supportsSetVariable = true;
		response.body.supportsExceptionOptions = true;
		response.body.supportsExceptionInfoRequest = true;
		response.body.supportsSetExpression = true;
		// response.body.exceptionBreakpointFilters = [
		// 	<DebugProtocol.ExceptionBreakpointsFilter>{
		// 		filter: "All",
		// 		label: "All"
		// 	}
		// ]


		this.sendResponse(response);

		// since this debug adapter can accept configuration requests like 'setBreakpoint' at any time,
		// we request them early by sending an 'initializeRequest' to the frontend.
		// The frontend will end the configuration sequence by calling 'configurationDone' request.
		this.sendEvent(new InitializedEvent());
	}

	/**
	 * Called at the end of the configuration sequence.
	 * Indicates that all breakpoints etc. have been sent to the DA and that the 'launch' can start.
	 */
	protected configurationDoneRequest(response: DebugProtocol.ConfigurationDoneResponse, args: DebugProtocol.ConfigurationDoneArguments): void {
		super.configurationDoneRequest(response, args);

		// notify the launchRequest that configuration has finished
		this._configurationDone.notify();
	}

	protected async launchRequest(response: DebugProtocol.LaunchResponse, args: LaunchRequestArguments) {

		// make sure to 'Stop' the buffered logging if 'trace' is not set
		logger.setup(args.trace ? Logger.LogLevel.Verbose : Logger.LogLevel.Stop, false);

		// wait until configuration has finished (and configurationDoneRequest has been called)
		await this._configurationDone.wait(1000);

		// start the program in the runtime
		this._runtime.start(args.program, !!args.stopOnEntry);

		this.sendResponse(response);
	}

	protected setBreakPointsRequest(response: DebugProtocol.SetBreakpointsResponse, args: DebugProtocol.SetBreakpointsArguments): void {

		const path = <string>args.source.path;
		const clientLines = args.lines || [];

		// clear all breakpoints for this file
		this._runtime.clearBreakpoints(path);

		// set and verify breakpoint locations
		const actualBreakpoints = clientLines.map(l => {
			let { verified, line, id } = this._runtime.setBreakPoint(path, this.convertClientLineToDebugger(l));
			const bp = <DebugProtocol.Breakpoint> new Breakpoint(verified, this.convertDebuggerLineToClient(line));
			bp.id= id;
			return bp;
		});

		if(this._runtime._childProcess){
			this._runtime._childProcess.stdin.write('set_brake_points(' + args.lines?.join() + ')\n');
		}

		// send back the actual breakpoint positions
		response.body = {
			breakpoints: actualBreakpoints
		};
		this.sendResponse(response);
	}

	protected breakpointLocationsRequest(response: DebugProtocol.BreakpointLocationsResponse, args: DebugProtocol.BreakpointLocationsArguments, request?: DebugProtocol.Request): void {

		if (args.source.path) {
			const bps = this._runtime.getBreakpoints(args.source.path, this.convertClientLineToDebugger(args.line));
			response.body = {
				breakpoints: bps.map(col => {
					return {
						line: args.line,
						column: this.convertDebuggerColumnToClient(col)
					}
				})
			};
		} else {
			response.body = {
				breakpoints: []
			};
		}
		this.sendResponse(response);
	}

	protected threadsRequest(response: DebugProtocol.ThreadsResponse): void {

		// runtime supports no threads so just return a default thread.
		response.body = {
			threads: [
				new Thread(SwodlDebugSession.THREAD_ID, "thread 1")
			]
		};
		this.sendResponse(response);
	}

	protected stackTraceRequest(response: DebugProtocol.StackTraceResponse, args: DebugProtocol.StackTraceArguments): void {

		const startFrame = typeof args.startFrame === 'number' ? args.startFrame : 0;
		const maxLevels = typeof args.levels === 'number' ? args.levels : 1000;
		const endFrame = startFrame + maxLevels;

		const stk = this._runtime.stack(startFrame, endFrame);

		response.body = {
			stackFrames: stk.frames.map(f => new StackFrame(f.index, f.name, this.createSource(f.file), this.convertDebuggerLineToClient(f.line))),
			totalFrames: stk.count
		};
		this.sendResponse(response);
	}

	protected scopesRequest(response: DebugProtocol.ScopesResponse, args: DebugProtocol.ScopesArguments): void {

		response.body = {
			scopes: [
				//new Scope("Local", this._variableHandles.create("local"), false)
				new Scope("Global", this._variableHandles.create("global"), false),
				new Scope("Functions", this._variableHandles.create("functions"), false)
			]
		};
		this.sendResponse(response);
	}

	private createVar(name: string, obj: any, i: number): never {
		var ref, v;
		var t = typeof(obj);
		if(t == 'object'){
			//i += 1;
			//v = JSON.stringify(obj);
			ref = i;
		} else {
			//v = obj;
			ref = 0;
		}
		return {
			name: name,
			value: JSON.stringify(obj),//v,
			type: "string",
			variablesReference: ref
		}
	}

	private flatten(obj, ans, path="") {
		for (var x in obj) {
			var tpath = path + "/" + x
			ans.push({
				path: tpath,
				name: x,
				index: ans.length,
				value: obj[x]});
			if (typeof obj[x] === 'object') {
				this.flatten(obj[x], ans, tpath);
			}
		}
	  }

	protected async variablesRequest(response: DebugProtocol.VariablesResponse, args: DebugProtocol.VariablesArguments, request?: DebugProtocol.Request) {

		const variables: DebugProtocol.Variable[] = [];

		if (this._isLongrunning.get(args.variablesReference)) {
			// long running

			if (request) {
				this._cancelationTokens.set(request.seq, false);
			}

			for (let i = 0; i < 100; i++) {
				await timeout(1000);
				variables.push({
					name: `i_${i}`,
					type: "integer",
					value: `${i}`,
					variablesReference: 0
				});
				if (request && this._cancelationTokens.get(request.seq)) {
					break;
				}
			}

			if (request) {
				this._cancelationTokens.delete(request.seq);
			}

		}
		var valuesGlobal = JSON.parse(this._runtime.variables[0].value);
        if (this._runtime.variables.length > 1) {
            var valuesFunc = JSON.parse(this._runtime.variables[1].value);
        }
		var values = valuesGlobal;
		var path = "/";
		var ca = []
		this.flatten(valuesGlobal, ca);
		if (args.variablesReference < 1000) {
			//var i = args.variablesReference + 1;
			var key = ca[args.variablesReference];
			values = key.value;//valuesGlobal[key];
			path = key.path + "/";
		} else {
			//var i = 10000;
			//var values = valuesFunc;
			//var index = args.variablesReference - 1000;
		}
		//if(args.count == undefined) {
		// for (var x in valuesGlobal) {
		// 	//x.index = i;
		// 	i++;
		// }
		// var vv = [];
		// this.flatten(values, vv);
		var a = [];
		for(var k in values){
			//if(vv[k])
			var c = ca.filter(el => el.path == path + k)[0];
			a.push(this.createVar(c.name,
				c.value,
				c.index));
		}
		// for(var k in ca){
		// 	if (values.hasOwnProperty(ca[k].name)){
		// 		a.push(this.createVar(ca[k].name,
		// 			ca[k].value,
		// 			ca[k].index));
		// 	//i++;
		// 	}
		// }
		response.body = {
			variables: a
		};
		// } else{
		// 	var v = JSON.parse(this._runtime.variables[args.variablesReference].value);
		// 	var a = [];
		// 	for(var i in v){
		// 		a.push({
		// 			name: i,
		// 			value: JSON.stringify(v[i]),
		// 			type: "string",
		// 			variablesReference: 1
		// 		});
		// 	}
		// 	response.body = {
		// 		variables: a
		// 	};
		// }
		this.sendResponse(response);
	}

	protected setVariableRequest(response: DebugProtocol.SetVariableResponse, args: DebugProtocol.SetVariableArguments, request?: DebugProtocol.Request) {
		this._runtime.addVar(args.name, args.value, "string");
		this._runtime._childProcess.stdin.write(`${args.name}=${args.value}\n`);
		response.body = {value: args.value};
		this.sendResponse(response);
		this.variablesRequest(<DebugProtocol.VariablesResponse>{}, args, request)
	}

	protected continueRequest(response: DebugProtocol.ContinueResponse, args: DebugProtocol.ContinueArguments): void {
		this._runtime.continue();
		this.sendResponse(response);
	}

	protected reverseContinueRequest(response: DebugProtocol.ReverseContinueResponse, args: DebugProtocol.ReverseContinueArguments) : void {
		this._runtime.continue(true);
		this.sendResponse(response);
 	}

	protected nextRequest(response: DebugProtocol.NextResponse, args: DebugProtocol.NextArguments): void {
		this._runtime.step();
		this.sendResponse(response);
	}

	// protected stepBackRequest(response: DebugProtocol.StepBackResponse, args: DebugProtocol.StepBackArguments): void {
	// 	this._runtime.step(true);
	// 	this.sendResponse(response);
	// }

	protected evaluateRequest(response: DebugProtocol.EvaluateResponse, args: DebugProtocol.EvaluateArguments): void {

		//let reply: string | undefined = undefined;

		var temp = JSON.parse(this._runtime.variables[0].value);
		// temp = temp[args.expression];
		// reply = temp ? temp.value : ''

		response.body = {
			result: JSON.stringify(temp[args.expression]),
			variablesReference: 0
		};
		this.sendResponse(response);
	}

	protected dataBreakpointInfoRequest(response: DebugProtocol.DataBreakpointInfoResponse, args: DebugProtocol.DataBreakpointInfoArguments): void {

		response.body = {
            dataId: null,
            description: "cannot break on data access",
            accessTypes: undefined,
            canPersist: false
        };

		if (args.variablesReference && args.name) {
			const id = this._variableHandles.get(args.variablesReference);
			if (id.startsWith("global_")) {
				response.body.dataId = args.name;
				response.body.description = args.name;
				response.body.accessTypes = [ "read" ];
				response.body.canPersist = true;
			}
		}

		this.sendResponse(response);
	}

	protected setDataBreakpointsRequest(response: DebugProtocol.SetDataBreakpointsResponse, args: DebugProtocol.SetDataBreakpointsArguments): void {

		// clear all data breakpoints
		this._runtime.clearAllDataBreakpoints();

		response.body = {
			breakpoints: []
		};

		for (let dbp of args.breakpoints) {
			// assume that id is the "address" to break on
			const ok = this._runtime.setDataBreakpoint(dbp.dataId);
			response.body.breakpoints.push({
				verified: ok
			});
		}

		this.sendResponse(response);
	}

	protected sendErrorResponse(response: DebugProtocol.Response, codeOrMessage: number | DebugProtocol.Message, format?: string, variables?: any, dest = 1)	{
		let msg : DebugProtocol.Message;
		if (typeof codeOrMessage === 'number') {
			msg = <DebugProtocol.Message> {
				id: <number> codeOrMessage,
				format: format
			};
			if (variables) {
				msg.variables = variables;
			}
			msg.showUser = true;
		} else {
			msg = codeOrMessage;
		}

		response.success = false;
		response.message = msg.format;//DebugSession.formatPII(msg.format, true, msg.variables);
		if (!response.body) {
			response.body = { };
		}
		response.body.error = msg;
		this.sendResponse(response);
	}

	protected completionsRequest(response: DebugProtocol.CompletionsResponse, args: DebugProtocol.CompletionsArguments): void {

		response.body = {
			targets: [
				{
					label: "item 10",
					sortText: "10"
				},
				{
					label: "item 1",
					sortText: "01"
				},
				{
					label: "item 2",
					sortText: "02"
				}
			]
		};
		this.sendResponse(response);
	}

	protected cancelRequest(response: DebugProtocol.CancelResponse, args: DebugProtocol.CancelArguments) {
		if (args.requestId) {
			this._cancelationTokens.set(args.requestId, true);
		}
	}

	//---- helpers

	private createSource(filePath: string): Source {
		return new Source(basename(filePath), this.convertDebuggerPathToClient(filePath), undefined, undefined, 'mock-adapter-data');
	}
}

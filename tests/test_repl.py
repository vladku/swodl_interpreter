from click.testing import CliRunner
from swodl_interpreter.__main__ import main

comands = ['test', '4.5 + "test"', 'b = 4 - 9;', 'b', 'exit()']


def test_repl():
    result = CliRunner().invoke(main, ['repl'], input='\n'.join(comands))
    assert '4.5test\n-5\n' == result.output.replace('>', '').replace(
        'Set MAM host: ', ''
    )

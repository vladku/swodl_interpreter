import pytest
from swodl_interpreter.runner import Runner
from swodl_interpreter.web_service_resolver import Resolver


@pytest.fixture
def mock_web_services(monkeypatch):
    def web_call(s, path, name):
        if name == 'GetEMObjectWithLocations':
            return (
                lambda *args: '<root><orgpath>path</orgpath><carrierguid>id</carrierguid><x>x</x></root>'
            )
        if name == 'MultipleTest':
            return lambda *args: f'<root><c><a>1</a><b>{args}</b></c></root>'
        return lambda *args: '<-->'.join(args)

    monkeypatch.setattr(Resolver, 'get', web_call)


def test_declare(mock_web_services):
    out = Runner(
        """
    declare void Test();
    declare const boolean OMA_UpdateOrderState(orderID, state, accesskey) as UpdateState;
    declare [orgpath, carrierguid, x] GetEMObjectWithLocations(accesskey, emguid)
        as GetEMObjectWithLocations
        @ "EssenceManagerWS/EssenceManager";
    var path, carrier;
    [path, carrier, a] = GetEMObjectWithLocations("123", "123");
    emobj2 = UpdateState("123", "321") @ "TEST/test";
    t = Test() @@ "http//test";
    """
    ).run().Global
    assert 'path' == out['path']
    assert 'id' == out['carrier']
    assert '123<-->321' == out['emobj2']
    assert '' == out['t']


# test_declare()
def test_declare_struct_array():
    Runner('declare void Test(str{}, arr[]);').run()


def test_declare_array_with_type():
    Runner('declare soap _auto Test(a, string arr[]);').run()
    Runner('declare const soap Test(a, string arr[]);').run()


def test_declare_multiple_return_values(mock_web_services):
    out = Runner(
    r"""
    declare [a,b] MultipleTest(string arr[]);
    [some, another] = MultipleTest(["value", 2]) @ "TEST/test";
    """
    ).run().Global
    assert '1' == out['some']
    assert "(['value', 2],)" == out['another']


# from _pytest.monkeypatch import MonkeyPatch
# test_declare_multiple_return_values(mock_web_services(MonkeyPatch()))

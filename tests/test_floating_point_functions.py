from helper import interp


def test_float():
    out = interp(
        """
    x = sqrt(25); // square root (result: 5.0)
    x1 = abs(-2.0);// absolute value (result: 2.0)
    x2 = floor(2.5);// floor (result: 2.0)
    x3 = ceil(2.5);// ceiling (result: 3.0)
    x4 = pow(3.0, 4.0);// power (result: 81.0)
    x5 = round(3.5);// rounding (result: 4.0)
    nop();
    """
    )
    assert 5.0 == out['x']
    assert 2.0 == out['x1']
    assert 2.0 == out['x2']
    assert 3.0 == out['x3']
    assert 81.0 == out['x4']
    assert 4.0 == out['x5']

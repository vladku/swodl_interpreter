from swodl_interpreter.lexer import Lexer
from swodl_interpreter.parser import Parser
from swodl_interpreter.interpreter import Interpreter
from swodl_interpreter.storage import Scope


def test_lines():
    lexer = Lexer(
        """
    /*
    fasfas
    af
    */
    var numbers = "vasvsav
        avasv
        vasv";
    numbers = 6;
    // asvasvsv
    numbers = 7;

    gosub sub;

    sub:
        sub = 1;
        return;

    if(9 < 10){
        v = "test";
    }
    i = 0;
    while(i < 4){
        i += 1;
        v = "test" + 1;
    }
    var n = 1;
    do {
        print(1);
        n = n + 1;
    } while(n < 3);
    """
    )
    scope = Scope()
    parser = Parser(lexer, scope)
    i = Interpreter(parser, scope)
    steps = i.interpret()
    _r = []
    for line, _, _ in steps:
        _r.append(line)
    assert [
        6,
        6,
        9,
        11,
        13,
        15,
        16,
        17,
        15,
        16,
        17,
        19,
        20,
        19,
        22,
        23,
        24,
        25,
        23,
        24,
        25,
        23,
        24,
        25,
        23,
        24,
        25,
        23,
        27,
        28,
        29,
        30,
        28,
        29,
        30,
        28,
    ] == _r


def test_lines2():
    lexer = Lexer(
        r"""
        var str = "<AXFRoot><MAObject type=\"QueryResult\" mdclass=\"QueryResult\"><GUID dmname=\"\"/><Meta name=\"NUMBEROFHITS\" format=\"string\">6192</Meta><Meta name=\"FIRSTHIT\" format=\"string\">5</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">3b4b62f5-a1ac-4f40-8e8e-5dd1a23d3dd1</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">4</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">4ae6dca5-e52e-49fa-835d-45ceb9ba1ffc</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">5</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">8173d4a5-bd28-43c7-b422-87da613f1bc8</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">6</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">98eebab7-4025-4e2c-b1b3-e766f1f23d90</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">7</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">c1929f51-a389-4e24-bb0b-74687f94236e</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">8</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">76ca3ae7-e492-4567-9f22-6173ab7b0ee8</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">9</Meta></MAObject></AXFRoot>";
        var x = xml_select_multi(str, "/*[local-name()='AXFRoot']/*[local-name()='MAObject'][@mdclass='MEDIA']");
        var a = array_create();
        var i = 0;
        var size = array_size(x);
        if (true){
            var iii_if = 1;
        }
        while(i < (int)size)
        {
            var item = 0;
            var obj = x[i];

            var path = "/MAObject/GUID";
            item.Guid = xml_select(obj, path);
            item.Maintitle = xml_text(xml_select(obj, "/MAObject/Meta[@name='MAINTITLE']"));
            //item.Maintitle = xml_select(obj, "/MAObject/Meta[@name='MAINTITLE']");
            a = array_add(a, item);
            i = i + 1;
        }
        var aaaaa = a[0].Guid;
        do{
            i = i + 1;
        } while(i < 15);
        """
    )
    scope = Scope()
    parser = Parser(lexer, scope)
    i = Interpreter(parser, scope)
    steps = i.interpret()
    _r, _n = [], []
    for line, a, b in steps:
        _r.append(line)
        _n.append(b)
    assert -1 not in _r

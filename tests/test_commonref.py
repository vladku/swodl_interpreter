from helper import interp


def test_commonref():
    out = interp(
        """
    var urn = "urn:com.avid:common-object:interplay-mam:2348238472398:asset.VIDEO:23434-23F32-4234234-3243423";
    var systemType = commonref_systemtype(urn);
    var systemId = commonref_systemid(urn);
    var type = commonref_type(urn);
    var id = commonref_id(urn);
    var newUrn = commonref_urn("interplay", "12345", "asset", "ABCD");
    """
    )
    assert 'interplay-mam' == out['systemType']
    assert '2348238472398' == out['systemId']
    assert 'asset.VIDEO' == out['type']
    assert '23434-23F32-4234234-3243423' == out['id']
    assert 'urn:com.avid:common-object:interplay:12345:asset:ABCD' == out['newUrn']

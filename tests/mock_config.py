config = {
    'services': {
        'a': {
            'b': {
                'test': {
                    'cases': [{
                        'args': [
                            '1'
                        ],
                        'result': 'One'
                    }],
                    'default': 123
                }
            }
        }
    },
    'config': {
        'profile': {
            'section': {
                'section': {
                    'key': 'CONFIGURATION!'
                }
            }
        }
    }
}

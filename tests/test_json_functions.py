from helper import interp


def test_json_to_swodl():
    out = interp(
        """
    var json = "[" +
        " {" +
            " \\"name\\" : \\"John Doe\\"," +
            " \\"benefits\\": [" +
                " \\"Life Insurance\\"," +
                " \\"Disability Insurance\\"" +
            " ]" +
        " }," +
        " {" +
            " \\"name\\" : \\"Bob Smith\\"," +
            " \\"benefits\\": [" +
                " \\"Life Insurance\\"," +
                " \\"Disability Insurance\\"," +
                " \\"401K\\"" +
            " ]" +
        " }" +
    " ]";
    var employees = json_to_swodl(json);
    """
    )
    assert [
        {'name': 'John Doe', 'benefits': [
            'Life Insurance', 'Disability Insurance']},
        {
            'name': 'Bob Smith',
            'benefits': ['Life Insurance', 'Disability Insurance', '401K'],
        },
    ] == out['employees']


def test_swodl_to_json():
    out = interp(
        """
    var arr = array_create();
    arr[0].name = "John Doe";
    arr[0].benefits[0] = "Life Insurance";
    arr[0].benefits[1] = "Disability Insurance";
    arr[1].name = "Bob Smith";
    arr[1].benefits[0] = "Life Insurance";
    arr[1].benefits[1] = "Disability Insurance";
    arr[1].benefits[2] = "401K";
    var params;
    params.employees = arr;
    var json = swodl_to_json(params);
    """
    )
    assert (
        '{"employees": [{"name": "John Doe", "benefits": ["Life Insurance", "Disability Insurance"]}, {"name": "Bob Smith", "benefits": ["Life Insurance", "Disability Insurance", "401K"]}]}'
        == out['json']
    )

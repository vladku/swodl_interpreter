import pytest
import pkg_resources

from helper import interp


@pytest.fixture
def register_plugin():
    distribution = pkg_resources.Distribution(__file__)
    ep = pkg_resources.EntryPoint.parse(
        'fake=swodl_interpreter.fake:WebFake', dist=distribution
    )
    distribution._ep_map = {'swodl_service_resolvers': {'fake': ep}}
    pkg_resources.working_set.add(distribution, 'swodl_service_resolvers')


def test_regex_ismatch(register_plugin):
    out = interp(
        """
    var empty = config_get("DataManagerWS", "Logging/File/Level");
    var info = config_get("DataManagerWS", "Logging/File/Level", "Info");
    var test = config_get("Test", "Test");
    var test_def = config_get("Test", "Def", "Def");
    """
    )
    assert '' == out['empty']
    assert 'Info' == out['info']
    assert 'Test' == out['test']
    assert 'Def' == out['test_def']
